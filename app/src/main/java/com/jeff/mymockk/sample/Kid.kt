package com.jeff.mymockk.sample

class Kid(private val mother: Mother) {
    var money = 20
        private set

    fun wantMoney() {
        for (i in 0..0) {
            mother.inform(money)
        }
        money += mother.giveMoney()
    }
}