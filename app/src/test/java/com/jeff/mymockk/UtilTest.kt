package com.jeff.mymockk

import com.jeff.mymockk.sample2.Util
import com.jeff.mymockk.sample2.UtilKotlin2
import com.jeff.mymockk.sample2.UtilKotlin
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.mockkStatic
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Test

class UtilTest {

    // https://medium.com/joe-tsai/mockk-%E4%B8%80%E6%AC%BE%E5%BC%B7%E5%A4%A7%E7%9A%84-kotlin-mocking-library-part-4-4-f82443848a3a
    @Test
    fun test1() {
        // Given
        val util = Util()
        mockkStatic(UtilKotlin::class)
        every { UtilKotlin.ok() } returns "A"

        mockkObject(UtilKotlin2)
        mockkObject(UtilKotlin2.Companion)
        every { UtilKotlin2.ok() } returns "B"

        // When
        util.ok()

        // Then
        verify { UtilKotlin.ok() }
        verify { UtilKotlin2.ok() }

        assertEquals("A", UtilKotlin.ok())
        assertEquals("B", UtilKotlin2.ok())
    }

    @Test
    fun test2() {
        // Given
        val util = Util()
        mockkObject(UtilKotlin)
        every { UtilKotlin.ok() } returns "C"

        // When
        util.ok()

        // Then
        verify { UtilKotlin.ok() }
        assertEquals("C", UtilKotlin.ok())
    }
}