package com.jeff.mymockk

import com.jeff.mymockk.sample.Kid
import com.jeff.mymockk.sample.Mother
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class KidAnnotationTest {

    @MockK
    lateinit var mother: Mother

    lateinit var kid: Kid

    @Before
    fun setUp() {
        // 只要在 @Before 方法裡面加上 MockKAnnotations.init(this) 就能在外面用 Annotation 的方式 Mock 物件。
        MockKAnnotations.init(this, relaxUnitFun = true)
        kid = Kid(mother)
    }

    @Test
    fun wantMoney() {
        every { mother.giveMoney() } returns 40

        kid.wantMoney()

        assertEquals(30, kid.money)
    }
}