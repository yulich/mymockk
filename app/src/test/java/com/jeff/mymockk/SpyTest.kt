package com.jeff.mymockk

import io.mockk.mockk
import io.mockk.spyk
import org.junit.Assert
import org.junit.Test

class SpyTest {

    // https://www.jyt0532.com/2018/01/04/test-double-spy/
    // https://breeze924.github.io/2018/08/27/Mock%E5%92%8CSpy%E7%9A%84%E5%8C%BA%E5%88%AB/

    /**
     * Mock物件從頭到尾都是假的物件，當呼叫mock的方法時其實並不會做任何事，如果方法有返回值，呼叫後僅回傳null。
     * Spy物件幾乎同等於真的物件，當呼叫spy的方法時會呼叫真正的原方法，也會返回原方法執行後應返回的值。
     * 那spy物件和真的物件有什麼差別? 幹嘛不用真的物件就好了。
     * 因為真的物件並無法stub其方法，也無法追蹤其行為，所以要改用Spy物件來達成。
     * 在test中常可以看到stub或stubbing，其意思是模擬原方法的行為，例如原方法執行結果返回值為"100"，我們可以用stub將原方法的返回值改為"99"。
     * 所以在上面無論是stub mock或stub spy的方法，我們可以stub ArrayList.get(100)方法來模擬(自訂)返回的值。
     * 會用stub的時機通常都是單元測試時，被測試方法中呼叫了依賴物件的方法，所以要利用stub來模擬(或偽造)依賴物件方法的行為（及返回值），而不是真的去執行依賴方法的邏輯。
     * 只有在整合測試時才會去真的呼叫依賴物件的方法。
     */

    @Test
    fun fun1() {
        val list = mockk<ArrayList<String>>(relaxed = true)

        list.add("Jeff")

        Assert.assertEquals("Jeff", list[0])
    }

    @Test
    fun fun2() {
        val list = spyk<ArrayList<String>>()

        list.add("Jeff")

        Assert.assertEquals("Jeff", list[0])
    }
}