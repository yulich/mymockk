package com.jeff.mymockk

import com.jeff.mymockk.sample.Kid
import com.jeff.mymockk.sample.Mother
import io.mockk.*
import org.junit.Assert.assertEquals
import org.junit.Test

class KidTest {

    @Test
    fun wantMoney1() {
        // Given
        val mother = mockk<Mother>()
        val kid = Kid(mother)

        // 每當有人呼叫 mother.giveMoney() 這個方法，我就給他 30 元。你改變了這個方法行為。
        every { mother.giveMoney() } returns 30

        // When
        kid.wantMoney()
        // Then
        assertEquals(80, kid.money)
    }

    // https://medium.com/joe-tsai/mockk-%E4%B8%80%E6%AC%BE%E5%BC%B7%E5%A4%A7%E7%9A%84-kotlin-mocking-library-part-2-4-4be059331110
    @Test
    fun wantMoney2() {
        // When
        val mother = mockk<Mother>()
        val kid = Kid(mother)
        every { mother.giveMoney() } returns 30

        // 這是因為在 MockK 裡面，預設情況下 Mock 這個動作是很嚴謹的 ，你必須要指定所有的行為操作才行，在測試代碼前面加上下面那行：
        every { mother.inform(any()) } just Runs

        // Given
        kid.wantMoney()

        // Then
        // 當你想要驗證一段方法有沒有被呼叫到的時候可以使用 verify。
        verify { mother.inform(any()) }
        assertEquals(30, kid.money)
    }

    // https://medium.com/joe-tsai/mockk-%E4%B8%80%E6%AC%BE%E5%BC%B7%E5%A4%A7%E7%9A%84-kotlin-mocking-library-part-3-4-79b40fb73964
    @Test
    fun wantMoney3() {
        // When
        val mother = mockk<Mother>(relaxed = true)
        val kid = Kid(mother)
        every { mother.giveMoney() } returns 30

        // Given
        kid.wantMoney()

        // Then
        // 當你想要驗證一段方法有沒有被呼叫到的時候可以使用 verify。
        verify(exactly = 3) { mother.inform(any()) }
        assertEquals(30, kid.money)
    }

    @Test
    fun wantMoney4() {
        // When
        val mother = mockk<Mother>(relaxed = true)
        val kid = Kid(mother)
        every { mother.giveMoney() } returns 30

        // Given
        kid.wantMoney()

        // Then
        // 當你想要驗證一段方法有沒有被呼叫到的時候可以使用 verify。
        verify(exactly = 1) {
            mother.inform(any())
            mother.giveMoney()
        }
        assertEquals(30, kid.money)
    }

    @Test
    fun wantMoney5() {
        // When
        val mother = mockk<Mother>(relaxed = true)
        val kid = Kid(mother)
        every { mother.giveMoney() } returns 30

        // Given
        kid.wantMoney()

        // Then
        // verifySequence 規定 inform() 的下一個執行的方法一定要是 giveMoney()，否則測試失敗。
        verifySequence {
            mother.inform(any())
            mother.giveMoney()
        }

        // verifyOrder 條件比較寬鬆，inform() 只要在 giveMoney() 之前執行即可，
        verifyOrder {
            mother.inform(any())
            mother.giveMoney()
        }

        assertEquals(30, kid.money)
    }

    @Test
    fun wantMoney6() {

        // When
        val mother = mockk<Mother>(relaxed = true)
        val kid = Kid(mother)

        val slot = slot<Int>()
        every { mother.inform(capture(slot)) } just Runs

        kid.wantMoney()

        // Then
        assertEquals(10, slot.captured)
    }

    @Test
    fun wantMoney7() {
        // Given
        val mother = spyk(Mother())
        val kid = Kid(mother)

        // When
        kid.wantMoney()
        // Then
        assertEquals(100, kid.money)
    }
}